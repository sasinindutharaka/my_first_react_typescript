import axios from "axios";
import { IRole, IUser } from "../redux/types";

// const responseHandling = (response: AxiosResponse) => {
//     const responseObject: ResponseObject = {
//       data: response.data,
//       status: response.status,
//       token: response.headers['authorization'],
//     }
//     return responseObject
//   }


class ApiService {
    async apiGet(){
        return await axios.get("http://localhost:5000/users")
        .then(function (response){
            
            return response
        })
    }



    async apiPsot(user:IUser){
        return await axios.post("http://localhost:5000/users",user)
        .then(function (response){
            
            return response
        })
        
        .catch((error) => console.log(error))
    }
     
    async apiDelete(id:number){
        return await axios.delete(`http://localhost:5000/users/${id}`)
        .then(function (response){
            
            return response
        })
        
        .catch((error) => console.log(error))
    }
    async apiRolePsot(role:IRole){
        return await axios.post("http://localhost:8080/user-management/role-and-permission/",role)
        .then(function (response){
            
            return response
        })
        
        .catch((error) => console.log(error))
    }


}

export default new ApiService();

