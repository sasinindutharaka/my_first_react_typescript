import apiService from './api'
import { IUser } from "../redux/types";

class UserDataService {
    
    async apiUserGet(){
        return apiService.apiGet();
    }

    async apiUserPost(user:IUser) {
    
        return apiService.apiPsot(user)
      }

    async apiUserDelete(id:number) {
    
        return apiService.apiDelete(id)
      }
}

export default new UserDataService();