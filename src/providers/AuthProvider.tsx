import React, { FC, createContext, useContext, useState } from 'react'


interface IAuthContext {
  isLoading: boolean
  isAuthenticate: boolean
  error: string

}

const AuthContext = createContext<IAuthContext>({
  isLoading: true,
  isAuthenticate: true,
  error: '',
 
})

export const useAuth = () => {
  return useContext(AuthContext)
}

export const AuthProvider: FC = ({ children }) => {
  const [isAuthenticate, setIsAuthenticate] = useState(true)
  const [isLoading, setIsLoading] = useState(true)
  const [error, setError] = useState('')





  return (
    <AuthContext.Provider
      value={{
        isLoading: isLoading,
        isAuthenticate: isAuthenticate,
        error: error
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
