import * as React from 'react'
import { useEffect } from 'react'
import { Route, Redirect, RouteProps } from 'react-router-dom'
import { useAuth } from '../providers/AuthProvider'

interface PrivateRouteProps extends RouteProps {
  component: any
}

const PrivateRoute = (props: PrivateRouteProps) => {
  const { component: Component, ...rest } = props
  const { isLoading, isAuthenticate} = useAuth()

 

  return (
    <Route
      {...rest}
      render={(routeProps) => {
        if (isLoading) {
          return isAuthenticate ? (
            <Component {...routeProps} />
          ) : (
            <Redirect
              to={{
                pathname: '/',
                state: { from: routeProps.location },
              }}
            />
          )
        }
      }}
    />
  )
}

export default PrivateRoute
