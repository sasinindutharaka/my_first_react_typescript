import React from "react";
import logo from "./logo.svg";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Headerbar from "./componants/Headerbar";
import Home from "./componants/Home";
import AddEditUser from "./componants/AddEditUser";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Singlepage from "./componants/Singlepage";
import RolesAndPermissions from "./componants/rolesAndPermissions/RolesAndPermissions";
import { AuthProvider } from "./providers/AuthProvider";
import PrivateRoute from "./providers/PrivateRouteProvider";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Headerbar />

        <AuthProvider>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/addUsers" component={AddEditUser} />
          <Route path="/edituser/:id" component={AddEditUser} />
          <Route path="/singlepage" component={Singlepage} />
          <PrivateRoute path="/roles" component={RolesAndPermissions} />
        </Switch>
        </AuthProvider>

      </BrowserRouter>
      
    </div>
  );
}

export default App;
