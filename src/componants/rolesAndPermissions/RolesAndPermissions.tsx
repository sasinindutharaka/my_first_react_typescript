import React from "react";
import { FormControl, Button, FormLabel,Container, Row, Col, Form, FormGroup } from "react-bootstrap";
import "./RolesAndPermissions.scss";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { createRoleRequest } from "../../redux/actions";

const initialState = {
  name : " ",
  description : " ",
  permissions : " ",
  };

const RolesAndPermissions = () => {
  const [formValue, setFormValue] = useState(initialState);

  const {name,description,permissions} = formValue;
    const dispatch = useDispatch();

    const handleInputchange = (e:any) => {
      let {name, value} = e.target;
          setFormValue({...formValue, [name]: value })
      };
  
  
  const handleSubmit = () => {
  
    dispatch(createRoleRequest(formValue));
    
    //console.log("form value is" + formValue.permissions)
      
  }

  return (
    <div className="div_style">
      <div className="div_header">
        <h3>Roles and Permissions</h3>
      </div>
      <br />
      
      <Form>
      <div className="div_title">
          <h5>Add Role</h5>
      </div>
      
      <br />

      <Container fluid>
      <Row>
        <Col className="col-4 div_title">
        <FormLabel>Name :</FormLabel>
        <br />
        <br />
        <br />
        <FormLabel>Discription :</FormLabel>
        <br />
        <br />
        
        <FormLabel>Permission :</FormLabel>
        </Col>
        <Col className="col ">
        <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="name" value={name} />
        <br />
        <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="description" value={description} />
        <br />
        <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="permissions" value={permissions} />
        </Col>
        
      </Row>
    </Container>
    <br/>
        <Form.Group className="mb-3 div_title" controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Application Register" name="permissions" />
    
  </Form.Group>
  
      <br />
      <div className="div_fotter">
        <Button className="mb-2" variant="primary" onClick={handleSubmit}>
          Save
        </Button>
        &nbsp;&nbsp;
        <Button className="mb-2" variant="primary">
          Cancel
        </Button>
      </div>
      </Form>
    </div>
  );
};

export default RolesAndPermissions;
