import { Form,Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux';
import React , { useState } from 'react'
import { createUserRequest } from '../redux/actions';
import { getLoadingSelector,getUserSelector,getErrorSelector } from '../redux/selector'
import { useSelector } from 'react-redux';

const initialState = {
    name : " ",
    email : " ",
    phone : " ",
    address : " ",
    };
    

const AddEditUser = () => {

    const [formValue, setFormValue] = useState(initialState);
    const users  = useSelector(getUserSelector)

    const[editMode, seteditMode] = useState(false);
    const {name,email,phone, address} = formValue;
    const dispatch = useDispatch();
    // const history = useHistory();
    // const {id} = useParams();
    // const {users} = useSelector((state) => state.data )

const handleInputchange = (e:any) => {
    let {name, value} = e.target;
        setFormValue({...formValue, [name]: value })
    };


const handleSubmit = () => {

    dispatch(createUserRequest(formValue));
    console.log(users)
    
}


    return (
        <div className="container">
      <div className="w-50 mx-auto shadow p-5 ">
        <h2 className="text-center mb-4">Add New User</h2>


<Form >
  <Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
    
  <Form.Label>Enter name :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="name" value={name} />
    
    <Form.Label>Enter Email :</Form.Label>
    <Form.Control type="email"  className="mb-2" onChange={handleInputchange} name="email" value={email} />

    <Form.Label>Contact NO :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="phone"value={phone} />
    
    <Form.Label>Enter Address :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={handleInputchange} name="address" value={address} />
   
    
  </Form.Group>

  
  <Button className="mb-3 " variant="primary" onClick={handleSubmit} >Add User</Button> 
  
  
  
</Form>
</div>
</div>
    )
}

export default AddEditUser
