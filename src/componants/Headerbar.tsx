import React from 'react'
import { Button, Container, Navbar, Nav } from 'react-bootstrap'
import { Link, NavLink, useHistory } from 'react-router-dom';

const Headerbar = () => {
  const history = useHistory()  
  return (
        <div>
            <Navbar bg="dark" variant="dark">
    <Container>
    <Navbar.Brand href="/">USER MANAGEMENT APPLICATION</Navbar.Brand>
    <Nav className="me-auto">
     
      <Nav.Link href="/">
        Home
      </Nav.Link >
      
      <Nav.Link href="/addusers">
        Add Users
      </Nav.Link>
      
      <Nav.Link href="/singlepage">
        Single Page
      </Nav.Link>

      <Nav.Link href="/roles">
        Roles And Permision
      </Nav.Link>
      
      <Nav.Link href="/auth">
        Auth
      </Nav.Link>
    </Nav>
    </Container>
  </Navbar>
        </div>
    )
}

export default Headerbar
