import React, { FC ,useEffect } from 'react'

import { useDispatch, useSelector } from 'react-redux'

import { deleteUserRequest, loadUserStart } from '../redux/actions'

import { getLoadingSelector,getUserSelector,getErrorSelector } from '../redux/selector'

import { Table } from 'react-bootstrap'
 
import {Link} from 'react-router-dom'

import './mystyle.scss'



const UsersPageComponant: FC = () => {
    const dispatch = useDispatch()
    const loading = useSelector(getLoadingSelector)
    const users  = useSelector(getUserSelector)
    const error = useSelector(getErrorSelector)   

    useEffect(() => {
       dispatch(loadUserStart())
       console.log(users)
    }, [dispatch])

    const handleDelete = (id: number) => {
      window.confirm("Are You sure...?")
      dispatch(deleteUserRequest(id))
    }
    return (
        <div className="">
        <div className=" ">
        {/* <div className="div_style shadow p-5"> */}
        <div>
            <Table>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Address</th>
      <th>Actions</th>
    </tr>
  </thead>
  {users.map((user, index) =>(
  <tbody>
    <tr>
      <th  scope="row">{index + 1}</th>
      <td className="td">{user.name}</td>
      <td className="td">{user.email}</td>
      <td className="td">{user.phone}</td>
      <td className="td">{user.address}</td>
      <td className="td"> 
      
      <button  className="btn btn-warning">Edit</button>
      
      
      &nbsp;&nbsp;
      <button  className="btn btn-danger" key={`button-${user.id}`} onClick={() => handleDelete( user.id ? user.id:0 )}>Delete</button>
      </td>
    </tr>
  </tbody>
  ))}
</Table>
        </div>
        </div>
        </div>
    )
}

export default UsersPageComponant

