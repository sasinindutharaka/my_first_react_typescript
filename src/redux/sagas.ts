import {
    take,
    takeEvery,
    takeLatest,
    put,
    all,
    delay,
    fork,
    call,
    } from "redux-saga/effects"

import { 
    
    loadUserSuccess,
    loadUserError,
    createUserSuccess,
    createUserFailure,
    deleteUserSuccess
 } from "./actions";

 import { CREATE_USER_START, DELETE_USER_START, LOAD_USERS_START } from "./actionTypes";

 

 import userService from "../apiManage/userService"
import { IUser } from "./types";
import axios, { AxiosResponse } from "axios";

 


/*
  Worker Saga: Fired on FETCH_TODO_REQUEST action
*/
function* LoadUserSagaWorker() {
    const { data, errorMessage } = yield call(userService.apiUserGet)
    
    if (data)
      yield put(
        loadUserSuccess({
          users: data,
        })
      )
    else
      yield put(
        loadUserError({
          error: errorMessage,
        })
      )
  }
  
  function* loadUserSaga() {
    yield all([takeLatest(LOAD_USERS_START, LoadUserSagaWorker)])
  }


  
  function* createUserSagaWorker (payload:any) {
    try {
      const response:AxiosResponse  = yield userService.apiUserPost(payload.user)
        if(response ){
            yield put(
              createUserSuccess({
                user:response.data
              })
              )
            
        }
    }catch(error) {
        console.log(error)
    }
}


  function* onCreateUser() {
    yield takeLatest(CREATE_USER_START, createUserSagaWorker)
}


function* deleteUserSagaWorker (payload:any) {
  try {
    const response:AxiosResponse  = yield userService.apiUserDelete(payload.id)
      if(response ){
          yield put(
            deleteUserSuccess({
              id:payload.id
            })
            )
          
      }
  }catch(error) {
      console.log(error)
  }
}



function* onDeleteUser() {
  yield takeLatest(DELETE_USER_START, deleteUserSagaWorker)
}

  

  export default function* userSaga() {
    yield all([fork(loadUserSaga), fork(onCreateUser),fork(onDeleteUser)])
  }