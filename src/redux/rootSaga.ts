import { all, fork } from 'redux-saga/effects'

import userSaga from './sagas'
import roleSaga from './roleSaga'


export function* rootSaga() {
    yield all([fork(userSaga),fork(roleSaga)])
  }