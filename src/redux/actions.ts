import {
    LOAD_USERS_START,
    LOAD_USERS_SUCCESS,
    LOAD_USERS_ERROR,
    CREATE_USER_START,
    CREATE_USER_SUCCESS,
    CREATE_USER_ERROR,
    DELETE_USER_SUCCESS,
    DELETE_USER_START,
    DELETE_USER_ERROR,
    CREATE_ROLE_START,
    CREATE_ROLE_SUCCESS,
    CREATE_ROLE_ERROR
} from "./actionTypes"

import { 
    LoadUserStart,
    LoadUserSuccess,
    LoadUserError,
    LoadUserSuccessPayload,
    LoadUserErrorPayload,
    IUser,
    CreateUserStart,
    CreateUserSuccessPayload,
    CreateUserSuccess,
    CreateUserErrorPayload,
    CreateUserError,
    DeleteUserSuccessPayload,
    DeleteUserSuccess,
    DeleteUserStart,
    DeleteUserErrorPayload,
    DeleteUserError,
    IRole,
    CreateRoleStart,
    CreateRoleSuccessPayload,
    CreateRoleSuccess,
    CreateRoleErrorPayload,
    CreateRoleError
 } from "./types";

 export const createRoleRequest = (role: IRole): CreateRoleStart => ({
    
  type: CREATE_ROLE_START,
  
  role,
  
})

export const createRoleSuccess = (payload: CreateRoleSuccessPayload): CreateRoleSuccess => ({
  type: CREATE_ROLE_SUCCESS,
  payload,
})

export const createRoleFailure = (payload: CreateRoleErrorPayload): CreateRoleError => ({
  type: CREATE_ROLE_ERROR,
  payload,
}) 






 export const loadUserStart = (): LoadUserStart => ({
    type: LOAD_USERS_START,
  })
  

  export const loadUserSuccess = (payload: LoadUserSuccessPayload): LoadUserSuccess => ({
    type: LOAD_USERS_SUCCESS,
    payload,
  })

  export const loadUserError = (payload: LoadUserErrorPayload): LoadUserError => ({
    type: LOAD_USERS_ERROR,
    payload,
  })

  export const createUserRequest = (user: IUser): CreateUserStart => ({
    
    type: CREATE_USER_START,
    
    user,
    
  })
  
  export const createUserSuccess = (payload: CreateUserSuccessPayload): CreateUserSuccess => ({
    type: CREATE_USER_SUCCESS,
    payload,
  })
  
  export const createUserFailure = (payload: CreateUserErrorPayload): CreateUserError => ({
    type: CREATE_USER_ERROR,
    payload,
  }) 


  export const deleteUserRequest = (id: number): DeleteUserStart => ({
    type: DELETE_USER_START,
    id,
  })
  
  export const deleteUserSuccess = (
    payload: DeleteUserSuccessPayload
  ): DeleteUserSuccess => ({
    type: DELETE_USER_SUCCESS,
    payload,
  })
  
  export const deleteUserFailure = (
    payload: DeleteUserErrorPayload
  ): DeleteUserError => ({
    type: DELETE_USER_ERROR,
    payload,
  })
// export const createUserStart = (user) =>({
//     type: types.CREATE_USER_START,
//     payload: user, 
// });

// export const createUserSuccess = () =>({
//     type : types.CREATE_USER_SUCCESS,
    
// });

// export const createUserError = (error) =>({
//     type: types.CREATE_USER_ERROR,
//     payload: error,
// });

// export const deleteUserStart = (userid) =>({
//     type: types.DELETE_USER_START,
//     payload: userid, 
// });

// export const deleteUserSuccess = (userid) =>({
//     type : types.DELETE_USER_SUCCESS,
//     payload: userid, 
    
// });

// export const deleteUserError = (error) =>({
//     type: types.DELETE_USER_ERROR,
//     payload: error,
// });


// export const updateUserStart = (userInfo) =>({
//     type: types.UPDATE_USER_START,
//     payload: userInfo, 
// });

// export const updateUserSuccess = () =>({
//     type : types.UPDATE_USER_SUCCESS,
    
    
// });

// export const updateUserError = (error) =>({
//     type: types.CREATE_USER_ERROR  ,
//     payload: error,
// });