import { createSelector } from 'reselect'

import { AppState } from './rootReducers'

const getLoading = (state: AppState) => state.userdata.loading

const getUsers = (state: AppState) => state.userdata.users

const getError = (state: AppState) => state.userdata.error

export const getUserSelector  = createSelector(getUsers, (users) => users)

export const getLoadingSelector = createSelector(getLoading, (loading) => loading)

export const getErrorSelector = createSelector(getError, (error) => error)

