import { combineReducers } from 'redux'

import usersReducers from './reducers'
import rolesReducers from './roleReducer'

const rootReducer = combineReducers({
  
  userdata: usersReducers,
  roledata: rolesReducers,
  
})

export type AppState = ReturnType<typeof rootReducer>

export default rootReducer
