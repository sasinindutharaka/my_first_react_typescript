import { 
    LOAD_USERS_START,
    LOAD_USERS_SUCCESS,
    LOAD_USERS_ERROR, 
    CREATE_USER_START,
    CREATE_USER_SUCCESS,
    CREATE_USER_ERROR,
    DELETE_USER_START,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR
} from "./actionTypes";

import produce from "immer";

import { UserActions, UserState } from "./types";

const initialState : UserState = {
    users: [],
    loading: false,
    error: null,
    
}


const usersReducers = (state = initialState, action: UserActions)  => {
    switch (action.type) {
        case LOAD_USERS_START:
          return produce(state, (draft) => {  
            draft.loading = true
          })
        case LOAD_USERS_SUCCESS:
          return produce(state, (draft) => {
            draft.loading = false
            draft.users = action.payload.users
            draft.error = null
          })
        case LOAD_USERS_ERROR:
          return produce(state, (draft) => {
            draft.loading= false
            draft.users = []
            draft.error = action.payload.error
          })
        case CREATE_USER_START:
            return produce(state, (draft) => {
              draft.loading = true
              
            })
        case CREATE_USER_SUCCESS:
          
            return produce(state, (draft) => {
              draft.loading = false
              draft.users.push(action.payload.user)
              draft.error = null
            })
            
        case CREATE_USER_ERROR:
            return produce(state, (draft) => {
              draft.loading = false
              draft.error = action.payload.error
            })

            case DELETE_USER_START:
      return produce(state, (draft) => {
        draft.loading = true
      })
    case DELETE_USER_SUCCESS:
      return produce(state, (draft) => {
        draft.loading = false
        draft.users.splice(
          draft.users.findIndex((user) => user.id === action.payload.id),
          1
        )
        draft.error = null
      })
    case DELETE_USER_ERROR:
      return produce(state, (draft) => {
        draft.loading = false
        draft.error = action.payload.error
      })
        
          default:
      return {
        ...state,
      }
}
};
export default usersReducers;