import { AxiosResponse } from "axios";
import { type } from "os";
import { 
    LOAD_USERS_START,
    LOAD_USERS_SUCCESS,
    LOAD_USERS_ERROR,
    CREATE_USER_START,
    CREATE_USER_SUCCESS,
    CREATE_USER_ERROR,
    DELETE_USER_START,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR,
    CREATE_ROLE_START,
    CREATE_ROLE_SUCCESS,
    CREATE_ROLE_ERROR
} from "./actionTypes";

export interface IUser{
    id?: number
    name: string
    email: string
    phone: string
    address: string

    
}
export interface IRole{
  
  name:string
  description: string
  permissions: string
}



export interface UserState {
    loading: boolean
    users: IUser[]
    error: string | null
}

export interface RoleState{
  roles: IRole[]
  loading: boolean
  error: string | null
}

export interface CreateRoleSuccessPayload {
  role: IRole
}

export interface CreateRoleErrorPayload {
  error: string
}

export interface CreateRoleStart{
    type: typeof CREATE_ROLE_START
    role: IRole
}

export interface CreateRoleSuccess{
  type: typeof CREATE_ROLE_SUCCESS
  payload: CreateRoleSuccessPayload
}
export interface CreateRoleError{
  type: typeof CREATE_ROLE_ERROR
  payload: CreateRoleErrorPayload
}







export interface LoadUserSuccessPayload {
    users: IUser[]
  }
  
export interface LoadUserErrorPayload {
    error: string
  }

export interface LoadUserStart{
      type: typeof LOAD_USERS_START
  }

export interface LoadUserSuccess{
    type: typeof LOAD_USERS_SUCCESS
    payload: LoadUserSuccessPayload
}
export interface LoadUserError{
    type: typeof LOAD_USERS_ERROR
    payload: LoadUserErrorPayload
}




export interface CreateUserSuccessPayload {
    user: IUser
  }
  
export interface CreateUserErrorPayload {
    error: string
  }

export interface CreateUserStart{
      type: typeof CREATE_USER_START
      user: IUser
  }

export interface CreateUserSuccess{
    type: typeof CREATE_USER_SUCCESS
    payload: CreateUserSuccessPayload
}
export interface CreateUserError{
    type: typeof CREATE_USER_ERROR
    payload: CreateUserErrorPayload
}





export interface DeleteUserSuccessPayload {
  id: number
}

export interface DeleteUserErrorPayload {
  error: string
}

export interface DeleteUserStart{
    type: typeof DELETE_USER_START
    id: number
}

export interface DeleteUserSuccess{
  type: typeof DELETE_USER_SUCCESS
  payload: DeleteUserSuccessPayload
}
export interface DeleteUserError{
  type: typeof DELETE_USER_ERROR
  payload: DeleteUserErrorPayload
}


export type UserActions =
  | LoadUserStart
  | LoadUserSuccess
  | LoadUserError
  | CreateUserStart
  | CreateUserSuccess
  | CreateUserError
  | DeleteUserStart
  | DeleteUserSuccess
  | DeleteUserError
  | CreateRoleStart

export type RoleAction = 
  | CreateRoleStart
  | CreateRoleSuccess
  | CreateRoleError