import {
    take,
    takeEvery,
    takeLatest,
    put,
    all,
    delay,
    fork,
    call,
    } from "redux-saga/effects"

import roleService from "../apiManage/roleService"

import axios, { AxiosResponse } from "axios";
import {  createRoleSuccess } from "./actions";
import { CREATE_ROLE_START } from "./actionTypes";

function* createRoleSagaWorker (payload:any) {
    try {
      const response:AxiosResponse  = yield roleService.apiUserPost(payload.role)
        if(response ){
            yield put(
              createRoleSuccess({
                role:response.data
              })
              )
            
        }
    }catch(error) {
        console.log(error)
    }
}


  function* onCreateRole() {
    yield takeLatest(CREATE_ROLE_START, createRoleSagaWorker)
}

export default function* roleSaga() {
    yield all([fork(onCreateRole)])
  }