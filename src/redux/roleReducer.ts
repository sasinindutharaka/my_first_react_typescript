import { RoleAction, RoleState} from "./types"
import produce from "immer";
import { CREATE_ROLE_ERROR, CREATE_ROLE_START, CREATE_ROLE_SUCCESS } from "./actionTypes";

const initialState : RoleState = {
    roles: [],
    loading: false,
    error: null,
    
}

const rolesReducers = (state = initialState, action: RoleAction)=>{
    switch(action.type) {
        case CREATE_ROLE_START:
            return produce(state, (draft) => {
              draft.loading = true
              
            })
        case CREATE_ROLE_SUCCESS:
          
            return produce(state, (draft) => {
              draft.loading = false
              draft.roles.push(action.payload.role)
              draft.error = null
            })
            
        case CREATE_ROLE_ERROR:
            return produce(state, (draft) => {
              draft.loading = false
              draft.error = action.payload.error
            })
        default:
            return {
                ...state,
            }
    }
}
export default rolesReducers